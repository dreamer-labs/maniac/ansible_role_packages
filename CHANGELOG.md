# [1.2.0](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/compare/v1.1.0...v1.2.0) (2020-03-18)


### Features

* Add retry logic to all types of instalation ([c84e6ca](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/commit/c84e6ca)), closes [#8](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/issues/8)

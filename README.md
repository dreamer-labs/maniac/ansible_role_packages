ansible_role_packages
=========

[![Build Status](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/badges/master/pipeline.svg)](https://gitlab.com/dreamer-labs/maniac/ansible_role_packages/pipelines)

Ansible role to install and updates packages required by roles

Using this role
--------------

The role needs to be loaded dynamically during the plays and the dictionary of parameters to pass to the role. Depending on the variables that are defined, it will install packages and perform a full system update if requested.

To perform a full system update during the running of the role, add the variable update_all_packages to the group or host vars.

Role Variables
--------------

This role will need to be provided with a dictionary of variables that have this structure.

``` yaml
update_all_packages: False
namespace_packages: # Namespace variable of the role
  yum: # name of package manager to use
    - package
      # Package to be installed, include version if
      # required
  apt:
    - package
```

Example Playbook
----------------

Example import:

``` yaml
---
- import_role:
    name: ansible_role_packages
  vars:
    packages: "{{ namespace_packages }}"
...
```

Example Vars:

``` yaml
update_all_packages: False
test_packages:
  yum:
    - "{{ yum packages }}"
  pip:
    - "{{ pypi_packages }}"
    update_cache: yes
```

License
-------

MIT

Author Information
------------------

Gloabl InfoTek, INC